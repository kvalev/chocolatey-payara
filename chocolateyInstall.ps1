$packageName = 'Payara'
$version = '4.1.2.181'
$url = "https://s3-eu-west-1.amazonaws.com/payara.fish/Payara+Downloads/Payara+$version/payara-$version.zip"
$checksum = '25252f6ec0be9fe1b7b14c4e03a2a982f90119590dcf185554a8e85becb248b6'

# download and unpack a zip file
Install-ChocolateyZipPackage `
    -PackageName "$packageName" `
    -Url "$url" `
    -UnzipLocation "$(Split-Path -parent $MyInvocation.MyCommand.Definition)" `
    -Checksum "$checksum" `
    -ChecksumType "sha256"