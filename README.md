# chocolatey-payara

[![Build status](https://ci.appveyor.com/api/projects/status/ykc0ekrdivjtcm7u?svg=true)](https://ci.appveyor.com/project/kvalev/chocolatey-payara)

This is a [Chocolatey](https://chocolatey.org/) package for the [Payara](http://www.payara.fish/) application server.

**Build the package**
```
choco pack
```

**Push the package**
```
choco apikey -k [API_KEY] -source https://chocolatey.org/
choco push -s https://chocolatey.org/
```